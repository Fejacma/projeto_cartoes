package br.com.itau.cartao.controllers;

import br.com.itau.cartao.dtos.PagamentoDTOEntradaPost;
import br.com.itau.cartao.models.Pagamento;
import br.com.itau.cartao.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    public ResponseEntity<Pagamento> postPagamento(@RequestBody PagamentoDTOEntradaPost pagamentoDTOEntradaPost) {
        Pagamento pagamento = new Pagamento();
        pagamento.setCartao(pagamentoDTOEntradaPost.getCartao());
        pagamento.setDescricao(pagamentoDTOEntradaPost.getDescricao());
        pagamento.setValor(pagamentoDTOEntradaPost.getValor());

        Pagamento pagamentoObjeto = pagamentoService.salvarPagamento(pagamento);

        return ResponseEntity.status(201).body(pagamentoObjeto);
    }

    @GetMapping("/{cartao_id}")
    public Iterable<Pagamento> getPagamentosByCartaoId(@PathVariable(name = "cartao_id") Long cartaoId) {
        Iterable<Pagamento> pagamentos = pagamentoService.consultarPagamentosPorCartaoId(cartaoId);
        return pagamentos;
    }

}
